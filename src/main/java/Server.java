import com.gabfusi.spaguetti.server.model.Game;
import com.gabfusi.spaguetti.server.networking.MessageReceiver;
import com.gabfusi.spaguetti.server.networking.socket.SocketServer;

import java.io.IOException;

public class Server {

    private static final int SERVER_PORT = 8081;
    private static final int MAX_CONNECTIONS = 16; // must be an even number

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        // Game engine will sends all outgoing socket messages
        Game.getInstance().start(MAX_CONNECTIONS/2);

        // MessageReceiver will parse all incoming socket messages
        MessageReceiver receiver = new MessageReceiver();

        // SocketServer will handle all incoming socket requests
        SocketServer server = new SocketServer(SERVER_PORT, MAX_CONNECTIONS, receiver);
        Game.getInstance().setSocketServer(server);

        // start server socket
        server.start();
        System.out.println("Server started.");
    }
}
