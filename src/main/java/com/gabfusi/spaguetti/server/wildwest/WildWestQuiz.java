package com.gabfusi.spaguetti.server.wildwest;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class WildWestQuiz {

    private ArrayList<Integer> askedQuestions = new ArrayList<>();
    private static String[][] qa = new String[10][4];
    private static Integer[] correct = new Integer[10];

    /**
     *
     */
    public WildWestQuiz() {

        qa[0][0] = "A Fistful of Dollars is an unofficial remake of this samurai film.";
        qa[0][1] = "The Sword of Doom";
        qa[0][2] = "Yojimbo";
        qa[0][3] = "The Hidden Fortress";
        correct[0] = 2;

        qa[1][0] = "In For a Few Dollars More Clint Eastwood was known as The Man With No Name.\nHis rival, Lee Van Cleef, was known as what?";
        qa[1][1] = "The Judge";
        qa[1][2] = "The Man in Black";
        qa[1][3] = "El Indio";
        correct[1] = 1;

        qa[2][0] = "In 1966’s Django directed by Sergio Corbucci, where does Django keep his guns?";
        qa[2][1] = "A Coffin";
        qa[2][2] = "A Railway Car";
        qa[2][3] = "A Streamer Trunk";
        correct[2] = 1;

        qa[3][0] = "Bernardo Bertolucci and this other famed Italian director contributed to the story of Sergio Leone’s Once Upon a Time in the West.";
        qa[3][1] = "Franco Zeffirelli";
        qa[3][2] = "Federico Fellini";
        qa[3][3] = "Dario Argento";
        correct[3] = 3;

        qa[4][0] = "What actor plays the \"Man With No Name\" in the \"Dollars\" trilogy?";
        qa[4][1] = "John Wayne";
        qa[4][2] = "Clint Eastwood";
        qa[4][3] = "Charles Bronson";
        correct[4] = 2;

        qa[5][0] = "What is the third and final film in Leone's \"Dollars\" trilogy?";
        qa[5][1] = "A Firstful of Dollars";
        qa[5][2] = "For a Few Dollars More";
        qa[5][3] = "The Good, the Bad and the Ugly";
        correct[5] = 3;

        qa[6][0] = "What year did \"A Fistful of Dollars\" premiere?";
        qa[6][1] = "1964";
        qa[6][2] = "1969";
        qa[6][3] = "1974";
        correct[6] = 1;

        qa[7][0] = "What is the nickname of Eastwood's character in \"For a Few Dollars More\"?";
        qa[7][1] = "Monco";
        qa[7][2] = "Blanco";
        qa[7][3] = "Diablo";
        correct[7] = 1;

        qa[8][0] = "Which of these titles does Eastwood's character take in \"The Good, the Bad, and the Ugly\"?";
        qa[8][1] = "The Good";
        qa[8][2] = "The Bad";
        qa[8][3] = "The Ugly";
        correct[8] = 1;

        qa[9][0] = "Who directed \"Django\"?";
        qa[9][1] = "Sergio Leone";
        qa[9][2] = "Ronald McDonald";
        qa[9][3] = "Sergio Corbucci";
        correct[9] = 3;

    }

    /**
     * Returns a random question
     *
     * @return
     */
    public JSONObject getRandomQuestion() {

        Random random = new Random();

        if (askedQuestions.size() == qa.length) {
            askedQuestions.clear();
        }

        int rand = random.nextInt(qa.length);
        while (askedQuestions.contains(rand)) {
            rand = random.nextInt(qa.length);
        }

        String[] question = qa[rand];
        askedQuestions.add(rand);

        JSONObject json = new JSONObject();
        JSONArray answers = new JSONArray();
        for (int i = 1; i < question.length; i++) {
            answers.put(question[i]);
        }
        json.put("questionId", getQuestionIdByString(question[0]));
        json.put("question", question[0]);
        json.put("answers", answers);

        return json;
    }

    /**
     * Check if an answer is correct
     *
     * @param question
     * @param answer
     * @return
     */
    public static boolean isAnswerCorrect(int question, int answer) {
        return correct[question] == answer;
    }

    /**
     * Returns a question id
     *
     * @param question
     * @return
     */
    private int getQuestionIdByString(String question) {
        for (int i = 0; i < qa.length; i++) {
            if (question.equals(qa[i][0])) {
                return i;
            }
        }
        return -1;
    }

}
