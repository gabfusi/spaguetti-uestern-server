package com.gabfusi.spaguetti.server.wildwest;


import java.util.*;

public class WildWestCityNames {

    static final List<String> list = Arrays.asList(
            "Longdale",
            "Stark Wood",
            "Cattlesprings",
            "Ironrange",
            "Swift Bank",
            "Devilcreak",
            "New Pass",
            "Longriver",
            "Stormvale",
            "Vapidcrag",
            "Lost Landing",
            "Overedge",
            "Slimstone",
            "Bonegorge",
            "Yellowpost",
            "Evil Tooth",
            "Flatpass",
            "Ghost's Creek",
            "Angel's Run",
            "Skullsnag",
            "Calico",
            "Jefferson",
            "El Paso",
            "Bodie",
            "Canyon Diablo",
            "Tucson"
    );

    /**
     * Returns random city names
     *
     * @param n
     * @return
     */
    public static List<String> getRandomNames(int n) {

        int max = list.size();

        if (n >= max) {
            n = max;
        }

        List<String> shuffled = list;
        Collections.shuffle(shuffled);

        return shuffled.subList(0, n);

    }

}
