package com.gabfusi.spaguetti.server.networking;

import com.gabfusi.spaguetti.server.model.Game;
import com.gabfusi.spaguetti.server.model.GameRoundState;
import com.gabfusi.spaguetti.server.model.Player;
import com.gabfusi.spaguetti.server.model.Team;
import com.gabfusi.spaguetti.server.networking.socket.MessageHandler;
import com.gabfusi.spaguetti.server.networking.socket.Connection;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.UUID;

/**
 * This is the socket I/O interface for message parsing
 */
public class MessageReceiver implements MessageHandler {

    public MessageReceiver() {}

    /**
     * On client login request
     *
     * @param connection
     * @param incomingMessagePayload
     */
    private void onLoginRequest(Connection connection, JSONObject incomingMessagePayload) {
        // subscribe a player to next game round

        UUID playerId = connection.getId();
        String nickname = incomingMessagePayload.getString("nickname");
        String teamString = incomingMessagePayload.getString("team");
        Team team = Team.valueOf(teamString);
        // attach connection id to incomingMessage (will be used to notify other players)
        incomingMessagePayload.put("id", playerId);

        // onLogin the user

        Game.getInstance().subscribePlayer(playerId, nickname, team);

        // create returning message
        Message returnMessage = new Message(MessageActions.LOGIN_ACK);
        // get round status (players, etc)
        JSONObject returnMessagePayload = Game.getInstance().getRoundStatusForPlayer(playerId);
        // add player id for this player
        returnMessagePayload.put("id", playerId);
        returnMessagePayload.put("next_round_at", Game.getInstance().getUpcomingRoundStartTick());
        returnMessagePayload.put("time", Game.getInstance().getGameTime());
        returnMessage.setPayload(returnMessagePayload);

        // return back the message to player
        connection.send(returnMessage.toJson());

        // notify all players that this player joined the Game.getInstance()
        Game.getInstance().notifyWaitingPlayers(MessageActions.PLAYER_JOINED, incomingMessagePayload);

    }

    /**
     * When a client request the game status
     *
     * @param connection
     */
    private void onGameStatusRequest(Connection connection) {
        // return game status
        // players position
        UUID playerId = connection.getId();
        JSONObject payload = Game.getInstance().getRoundStatusForPlayer(playerId);

        // create returning message
        Message returnMessage = new Message(MessageActions.GAME_STATUS);
        returnMessage.setPayload(payload);
        // write message to socket
        connection.send(returnMessage.toJson());
    }


    /**
     * A player want to move to a different city
     *
     * @param connection
     * @param incomingMessagePayload
     */
    public void onPlayerMoveRequest(Connection connection, JSONObject incomingMessagePayload) {
        // perform a player move to cityId

        UUID playerId = connection.getId();
        UUID playerId1 = UUID.fromString(incomingMessagePayload.getString("playerId"));
        UUID desiredCityId = UUID.fromString(incomingMessagePayload.getString("cityId"));

        if (!playerId.equals(playerId1)) {
            System.out.print("Request forged!");
            return;
        }

        Game.getInstance().movePlayer(playerId, desiredCityId); // may throws
    }

    /**
     * A player performed a battle (answered a question)
     *
     * @param connection
     * @param incomingMessagePayload
     */
    public void onFightPerformedRequest(Connection connection, JSONObject incomingMessagePayload) {
        // update player fight status

        UUID playerId = connection.getId();
        UUID playerId1 = UUID.fromString(incomingMessagePayload.getString("playerId"));
        int answerId = incomingMessagePayload.getInt("answerId");

        if (!playerId.equals(playerId1)) {
            System.out.print("Request forged!");
            return;
        }

        // client side answer is 0 index based.
        answerId = answerId + 1;

        Game.getInstance().addBattleToCurrentFight(playerId, answerId);
    }


    /**
     * Incoming message router
     *
     * @param connection
     * @param incomingMessage
     */
    private void performAction(Connection connection, Message incomingMessage) {

        try {

            JSONObject payload = incomingMessage.getPayload();

            switch (incomingMessage.getAction()) {

                case LOGIN:
                    onLoginRequest(connection, payload);
                    break;

                case GAME_STATUS: // unused
                    onGameStatusRequest(connection);
                    break;

                case PLAYER_MOVE:
                    onPlayerMoveRequest(connection, payload);
                    break;

                case FIGHT_PERFORMED:
                    onFightPerformedRequest(connection, payload);
                    break;

            }

        } catch (RuntimeException e) {

            e.printStackTrace();
            Message errorMessage = generateErrorMessage(e.getMessage());
            connection.send(errorMessage.toJson());
        }

    }


    /**
     * On client message received
     *
     * @param connection the socket connection (Connection)
     * @param json       the received message
     */
    @Override
    public void onReceive(Connection connection, String json) {

        // check if message is a valid json
        try {

            System.out.println("onReceive: received: " + json);

            // deserialize incoming message
            Message incomingMessage = deserializeMessage(json);
            if (incomingMessage == null) {
                throw new Exception("Not a valid json.");
            }

            // override user id to incoming message
            incomingMessage.setUserId(connection.getId()); // user and connection share the same id

            // perform the desired action
            performAction(connection, incomingMessage);


        } catch (IllegalArgumentException e) {

            //e.printStackTrace();
            System.out.println("onReceive: " + e.getMessage());

            Message errorMessage = generateErrorMessage(e.getMessage());
            connection.send(errorMessage.toJson());

        } catch (Exception e) {

            //e.printStackTrace();
            System.out.println("onReceive: socket message is not a valid json. " + json);

            Message errorMessage = generateErrorMessage("Received invalid json message.");
            connection.send(errorMessage.toJson());
        }

    }

    /**
     * When a client disconnects
     *
     * @param connectionId
     */
    @Override
    public void onClientDisconnected(UUID connectionId) {

        // get round status for disconnected player
        Player player = Game.getInstance().getPlayer(connectionId);

        if (player == null) {
            return;
        }

        JSONObject payload = new JSONObject();
        payload.put("id", connectionId);

        // unsubscribe player
        Game.getInstance().unsubscribePlayer(connectionId);


        // return message for all players inside its round
        if (player.getRoundState().equals(GameRoundState.MATCHMAKING)) {
            Game.getInstance().notifyWaitingPlayers(MessageActions.PLAYER_EXITED, payload);
        } else {
            Game.getInstance().notifyPlayingPlayers(MessageActions.PLAYER_EXITED, payload);
        }
    }

    /**
     * Deserialize an incoming message
     * (from json string to Message instance)
     *
     * @param message
     * @return Message
     */
    private Message deserializeMessage(String message) {

        if (message != null && !message.equals("") && !message.equals("null") && message.substring(0, 1).equals("{")) {

            return Message.fromJson(message);

        } else {
            return null;
        }
    }

    /**
     * @param errorString
     * @return
     */
    private Message generateErrorMessage(String errorString) {

        Message errorMessage = new Message(MessageActions.ERROR);
        JSONObject errorMessagePayload = new JSONObject();
        errorMessagePayload.put("message", errorString);
        errorMessage.setPayload(errorMessagePayload);
        return errorMessage;

    }

}