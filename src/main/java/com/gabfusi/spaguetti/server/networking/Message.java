package com.gabfusi.spaguetti.server.networking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class Message {
    private UUID userId = null;
    private MessageActions action;
    private JSONObject payload;

    public Message() {
    }

    /**
     * @param action
     */
    Message(MessageActions action) {
        this.action = action;
        this.payload = new JSONObject();
    }

    /**
     * @param userId
     */
    void setUserId(UUID userId) {
        this.userId = userId;
    }

    /**
     * @return
     */
    MessageActions getAction() {
        return action;
    }

    /**
     * @param action
     */
    public void setAction(MessageActions action) {
        this.action = action;
    }

    /**
     * @return
     */
    JSONObject getPayload() {
        return payload;
    }

    /**
     * @param payload
     */
    public void setPayload(JSONObject payload) {
        this.payload = payload;
    }

    /**
     * @return
     */
    public String toJson() {
        JSONObject source = new JSONObject();
        if (this.userId != null) {
            source.put("userId", this.userId.toString());
        }
        source.put("action", this.action);
        source.put("payload", this.payload);
        return source.toString();
    }

    /**
     * @param json
     * @return
     * @throws JSONException
     */
    static Message fromJson(String json) throws JSONException {

        JSONObject source = new JSONObject(json);

        if (!source.has("action")) {
            throw new JSONException("Received unrecognized message, no action specified.");
        }

        if (!source.has("payload")) {
            throw new JSONException("Received unrecognized message, no payload specified.");
        }

        MessageActions action = MessageActions.valueOf(source.getString("action"));
        JSONObject payload = source.getJSONObject("payload");

        Message message = new Message(action);
        message.setPayload(payload);

        return message;
    }
}
