package com.gabfusi.spaguetti.server.networking.socket;

import java.io.*;
import java.net.Socket;
import java.util.UUID;

/**
 * A ConnectionThread thread is spawned on each socket connection successful attempt
 */
class ConnectionThread extends Thread {
    private final UUID id;
    private final Socket socket;
    private final SocketServer socketServer;
    private final Connection connection;

    private volatile boolean isRunning;

    /**
     * Creates a connection thread
     *
     * @param connectionId is the uuid of the connection
     * @param socket       is a ServerSocket (java native) instance
     * @param socketServer is a SocketServer instance (SocketServer)
     */
    ConnectionThread(UUID connectionId, Socket socket, SocketServer socketServer) {
        this.id = connectionId;
        this.socket = socket;
        this.socketServer = socketServer;
        connection = new Connection(connectionId, socket);
        isRunning = true;
    }

    /**
     *
     * @return
     */
    public UUID getConnectionId() {
        return this.id;
    }

    @Override
    public void run() {
        while (isRunning) {
            // Check whether the socket is closed.
            if (socket.isClosed()) {
                isRunning = false;
                break; // implicity stops the current thread instance (run method finishes)
            }

            try {

                // create a new reader from the socket input stream
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                // read socket message
                String message = reader.readLine();

                // trigger onReceive callback on message received
                if (message != null) {
                    System.out.println("[" + this.id + "] received message " + message);
                    socketServer.getMessageHandler().onReceive(connection, message);
                } else {
                    System.out.println("[" + this.id + "] client disconnected");
                    stopRunning();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Send a message
     * @param message
     */
    void send(String message) {
        this.connection.send(message);
    }

    /**
     * close socket
     */
    void stopRunning() {
        isRunning = false;
        try {
            socketServer.getMessageHandler().onClientDisconnected(connection.getId());
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}