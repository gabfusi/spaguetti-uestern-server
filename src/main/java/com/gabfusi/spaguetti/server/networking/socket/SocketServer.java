package com.gabfusi.spaguetti.server.networking.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.UUID;

/**
 * SocketServer is threaded.
 * It creates one thread for accepting connections (ListeningThread)
 * and creates a new thread each time a new client is connected (ConnectionThread).
 * -> One request one thread.
 */
public class SocketServer {
    private ServerSocket serverSocket;
    private final MessageHandler messageHandler;
    private int maxConnections = 0;

    private volatile ListeningThread listeningThread;

    /**
     * Instantiate a new socket connection
     *
     * @param port        messages port
     * @param connections maximum simoultaneous connections
     * @param handler     handler
     */
    public SocketServer(int port, int connections, MessageHandler handler) {

        maxConnections = connections;
        messageHandler = handler;

        try {
            serverSocket = new ServerSocket(port, connections);
            listeningThread = new ListeningThread(this, serverSocket);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Start socket server
     */
    public void start() {
        listeningThread.start();
    }

    /**
     * Return the current message handler
     *
     * @return
     */
    MessageHandler getMessageHandler() {

        return messageHandler;
    }

    /**
     * Return the number of concurrently connected clients
     *
     * @return
     */
    public int getConnectionsNumber() {
        return listeningThread.getActualConnections();
    }

    /**
     * Return maximum allowed concurrent connections
     *
     * @return
     */
    int getMaxConnections() {
        return maxConnections;
    }

    /**
     * Broadcast a message (send a message to all connected clients)
     *
     * @param message
     */
    public void broadcast(String message) {
        listeningThread.broadcast(message);
    }

    /**
     * Send a message to a specific client
     *
     * @param connectionId
     * @param message
     */
    public void sendTo(UUID connectionId, String message) {
        listeningThread.sendTo(connectionId, message);
    }

    /**
     * Close socket
     */
    public void close() {

        try {
            if (serverSocket != null && !serverSocket.isClosed()) {
                listeningThread.stopRunning();
                //listeningThread.suspend();
                //listeningThread.stop();

                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}