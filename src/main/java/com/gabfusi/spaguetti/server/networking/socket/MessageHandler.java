package com.gabfusi.spaguetti.server.networking.socket;

import java.util.UUID;

/**
 * Handler interface
 * onReceive is triggered when the messages receive a new message
 */
public interface MessageHandler {
    void onReceive(Connection connection, String message);
    void onClientDisconnected(UUID connectionId);
}