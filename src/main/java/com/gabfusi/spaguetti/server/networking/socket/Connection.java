package com.gabfusi.spaguetti.server.networking.socket;

import java.net.Socket;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.UUID;

/**
 * Models a socket connection
 * An instance of this class is created by ConnectionThread
 * and it is available on onReceive callback
 */
public class Connection {
    private final UUID id;
    private final Socket socket;

    Connection(UUID id, Socket socket) {
        this.id = id;
        this.socket = socket;
    }

    public UUID getId() {
        return id;
    }

    /**
     * Print a message on the socket output stream
     * @param message
     */
    public void send(String message) {
        PrintWriter writer;
        try {
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            writer.println(message);
            System.out.println("[" + this.id + "] send: " + message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}