package com.gabfusi.spaguetti.server.networking.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ListeningThread is executed by SocketServer
 * One instance of SockeServer creates a ListeningThread thread.
 */
class ListeningThread extends Thread {
    private final SocketServer socketServer;
    private final ServerSocket serverSocket;
    // ConcurrentHashMap uses multiple locks on segment level (16 by default) instead of object level
    private volatile boolean isRunning;

    private final ConcurrentHashMap<UUID, ConnectionThread> connThreads;

    /**
     * Creates a listening thread
     *
     * @param socketServer is a SocketServer instance (SocketServer)
     * @param serverSocket is a ServerSocket (java native) instance
     */
    ListeningThread(SocketServer socketServer, ServerSocket serverSocket) {
        this.socketServer = socketServer;
        this.serverSocket = serverSocket;
        // a vector containing the spawned connectionThread(s)
        this.connThreads = new ConcurrentHashMap<>(socketServer.getMaxConnections(), 0.9f, socketServer.getMaxConnections());
        isRunning = true;
    }

    @Override
    public void run() {
        while (isRunning) {

            if (serverSocket.isClosed()) {
                isRunning = false;
                break;
            }

            try {

                Socket socket;
                socket = serverSocket.accept(); // blocks util a connection is made

                // creates a new connection thread
                UUID connectionId = UUID.randomUUID();
                ConnectionThread connection = new ConnectionThread(connectionId, socket, socketServer);

                ConnectionThread existing = connThreads.putIfAbsent(connectionId, connection);
                if(existing != null) {
                    throw new RuntimeException("Connection refused, you can't login twice.");
                }

                // start the connection thread
                connection.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Send a message to all connected clients
     *
     * @param message
     */
    void broadcast(String message) {

        connThreads.forEach((connectionId, connectionThread) -> {

            if (!connectionThread.getState().equals(State.TERMINATED)) {
                connectionThread.send(message);
            } else {
                connThreads.remove(connectionId);
            }

        });

    }

    /**
     * Send a message to a specific client
     *
     * @param connectionId
     * @param message
     */
    void sendTo(UUID connectionId, String message) {

        ConnectionThread connection = connThreads.get(connectionId);

        if (connection != null) {

            if (!connection.getState().equals(State.TERMINATED)) {
                connection.send(message);
            } else {
                connThreads.remove(connectionId);
            }

        }

    }

    /**
     * Return currently connected clients
     *
     * @return
     */
    int getActualConnections() {
        return connThreads.size();
    }

    /**
     * Stop all connectionThread(s) spawned
     */
    void stopRunning() { // never used
        isRunning = false;

        try {
            this.serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        connThreads.forEach((connectionId, connectionThread) -> {
            connectionThread.stopRunning();
            connThreads.remove(connectionId);
        });

    }
}