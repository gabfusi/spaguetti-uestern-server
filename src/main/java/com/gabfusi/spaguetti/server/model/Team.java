package com.gabfusi.spaguetti.server.model;

public enum Team {
    WHITE,
    BLACK
}