package com.gabfusi.spaguetti.server.model;

public enum GameRoundState {
    MATCHMAKING,
    IDLE,
    STARTED,
    ENDED
}
