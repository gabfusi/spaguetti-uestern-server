package com.gabfusi.spaguetti.server.model;

import com.gabfusi.spaguetti.server.wildwest.WildWestQuiz;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

class Fight {

    private final UUID cityId;
    private final int questionId;

    private final ConcurrentHashMap<UUID, Battle> battles = new ConcurrentHashMap<>();
    private final AtomicInteger battlesNumber = new AtomicInteger();

    /**
     * Models a Teams fight
     *
     * @param cityId
     * @param questionId
     */
    Fight(UUID cityId, int questionId) {
        this.cityId = cityId;
        this.questionId = questionId;
    }

    /**
     * Add a new Battle (Player's answer) in this fight
     *
     * @param player
     * @param answerId
     */
    void addBattle(Player player, int answerId) throws RuntimeException {
        Battle battle = new Battle(player, answerId);
        Battle existing = battles.putIfAbsent(player.getId(), battle);
        if (existing == null) {
            battlesNumber.getAndIncrement();
        } else {
            throw new RuntimeException("You can't answer a question twice.");
        }
    }

    /**
     *
     *
     * @return
     */
    int getBattlesPerformed() {
        return battlesNumber.get();
    }

    /**
     * compute the fight and return the winner team
     * it returns null in case of draw
     *
     * @return Team|null
     */
    Team getWinner() {

        int whiteTeamNumber = 0;
        int whiteTeamScore = 0;
        int blackTeamNumber = 0;
        int blackTeamScore = 0;

        for (Battle battle : battles.values()) {

            Player player = battle.getPlayer();
            boolean isAnswerCorrect = WildWestQuiz.isAnswerCorrect(questionId, battle.getAnswerId());

            if (player.getTeam().equals(Team.WHITE)) {
                whiteTeamNumber++;
                whiteTeamScore += isAnswerCorrect ? 1 : 0;
            } else {
                blackTeamNumber++;
                blackTeamScore += isAnswerCorrect ? 1 : 0;
            }

        }

        if (blackTeamNumber > 1 && blackTeamNumber == blackTeamScore) {
            blackTeamScore *= 2; // Bonus multiplier
        }

        if (whiteTeamNumber > 1 && whiteTeamNumber == whiteTeamScore) {
            whiteTeamScore *= 2; // Bonus multiplier
        }

        if (blackTeamScore == whiteTeamScore) {
            return null; // draw
        } else if (blackTeamScore > whiteTeamScore) {
            return Team.BLACK;
        } else {
            return Team.WHITE;
        }
    }

    /**
     * @return
     */
    UUID getCityId() {
        return cityId;
    }

    /**
     * Models a Player answer
     */
    public class Battle {
        private final Player player;
        private final int answerId;

        Battle(Player player, int answerId) {
            this.player = player;
            this.answerId = answerId;
        }

        Player getPlayer() {
            return player;
        }

        int getAnswerId() {
            return answerId;
        }
    }

}
