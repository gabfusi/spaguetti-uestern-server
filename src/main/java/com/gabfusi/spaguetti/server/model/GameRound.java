package com.gabfusi.spaguetti.server.model;

import com.gabfusi.spaguetti.server.wildwest.WildWestCityNames;
import com.gabfusi.spaguetti.server.networking.MessageActions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

class GameRound {

    private int roundDuration; // minutes
    private final World world = new World();
    private final ScheduledExecutorService stopper = Executors.newSingleThreadScheduledExecutor();
    private long timeStart;
    private long timeEnd;
    private final int maxPlayersPerRound;

    private volatile GameRoundState state = GameRoundState.MATCHMAKING;
    private volatile LinkedList<UUID> originalPlayersQueue;
    private volatile LinkedList<UUID> playersQueue;
    private volatile Fight currentFight;
    private final ConcurrentHashMap<UUID, Player> players;
    private final AtomicInteger playersNumber;
    private volatile UUID currentPlayingPlayerId;
    private volatile City uglyCurrentCity;

    private final Object playerRoundLock = new Object();

    /**
     * Models a Game round
     *
     * @param duration
     * @param maxPlayersPerRound
     */
    GameRound(int duration, int maxPlayersPerRound) {
        this.maxPlayersPerRound = maxPlayersPerRound;
        this.roundDuration = duration;
        this.originalPlayersQueue = new LinkedList<>();
        this.playersQueue = new LinkedList<>();
        this.playersNumber = new AtomicInteger();
        // see http://howtodoinjava.com/core-java/multi-threading/best-practices-for-using-concurrenthashmap/
        players = new ConcurrentHashMap<>(16, 0.9f, 1);
    }

    /**
     * Return Game Round status
     *
     * @return
     */
    JSONObject getStatus() {

        JSONObject status = new JSONObject();
        status.put("state", state);
        JSONArray players = new JSONArray();

        if (state.equals(GameRoundState.ENDED)) {

            for (Player player : getPlayers().values()) {
                JSONObject p = player.toJson();
                p.put("ammo", player.getAmmo());
                players.put(p);
            }

        } else {

            for (Player player : getPlayers().values()) {
                players.put(player.toJson());
            }
        }

        status.put("players", players);

        switch (state) {

            case STARTED:
                status.put("timeStart", timeStart);
                status.put("world", world.toJson());

                break;

            case ENDED:
                status.put("timeStart", timeStart);
                status.put("timeEnd", timeEnd);
                status.put("results", getRoundResults());

                break;

            case IDLE:
                status.put("timeStart", timeStart);
                break;

        }

        return status;
    }

    /**
     * return true if this round is started.
     *
     * @return
     */
    boolean isGameRoundStarted() {
        return state == GameRoundState.STARTED;
    }

    /**
     * Initialize a game round
     */
    boolean startGameRound() {

        if (state != GameRoundState.MATCHMAKING) {
            System.out.println("Round already started or ended");
            return false;
        }

        state = GameRoundState.IDLE;
        updatePlayersRoundState();

        // check if players are equally distributed in 2 teams
        // otherwise redistribute players and notify the shifted about the change
        if (!checkAndArrangePlayers()) {
            return false;
        }

        // generate game world
        System.out.println("Generating world...");
        generateWorld();

        // place player inside the generated world
        System.out.println("Populating world...");
        placePlayersInsideWorld();

        System.out.println("Starting round...");

        // start round timer
        startGameRoundTimer();
        timeStart = GameTime.getTime();

        state = GameRoundState.STARTED;
        updatePlayersRoundState();

        startUgly();

        return true;
    }

    /**
     * Terminate a game round
     */
    void stopGameRound() {

        silentStopGameRound();

        // Notify
        System.out.println("Round End!");
        Game.getInstance().notifyPlayingPlayers(MessageActions.ROUND_END, getStatus());
    }

    /**
     *
     */
    void silentStopGameRound() {

        if (state == GameRoundState.ENDED) {
            System.out.println("Round already ended");
            return;
        }

        state = GameRoundState.ENDED;
        updatePlayersRoundState();

        stopper.shutdown();
    }

    /**
     * Game game round timer
     */
    private void startGameRoundTimer() {

        stopper.schedule(roundStopper, roundDuration, TimeUnit.MINUTES);
    }

    /**
     * Timer task for round
     */
    private final TimerTask roundStopper = new TimerTask() {
        @Override
        public void run() {
            timeEnd = GameTime.getTime();
            stopGameRound();
        }
    };

    /**
     * Check if there are a sufficient number of players in this round,
     * and redistributes players equally in 2 teams
     *
     * @return boolean
     */
    private boolean checkAndArrangePlayers() {

        int blackTeamNumber = 0;
        int whiteTeamNumber = 0;
        int optimalTeamPlayersNumber = (int) Math.floor(playersNumber.get() / 2);
        int playersToMove;
        Stack<Player> blackTeam = new Stack<>();
        Stack<Player> whiteTeam = new Stack<>();

        if (playersNumber.get() < 2) {
            return false; // cannot play solo!
        }

        for (Player player : players.values()) {
            if (player.getTeam().equals(Team.BLACK)) {
                blackTeam.push(player);
                blackTeamNumber++;
            } else {
                whiteTeam.push(player);
                whiteTeamNumber++;
            }
        }

        // even number of players
        if (blackTeamNumber == whiteTeamNumber) {
            return true;
        }

        // odd number of players
        if (blackTeamNumber == optimalTeamPlayersNumber || whiteTeamNumber == optimalTeamPlayersNumber) {
            return true;
        }

        // players should be re-arranged
        System.out.println("Re-arranging players... black: " + blackTeamNumber + " white: " + whiteTeamNumber);

        if (blackTeamNumber > whiteTeamNumber) {
            // Move some black players to white team

            playersToMove = (int) Math.floor((blackTeamNumber - whiteTeamNumber) / 2);

            for (int i = 0; i < playersToMove; i++) {
                Player p = blackTeam.pop();
                System.out.println("Moving player " + p.getNickname() + " to white team");
                p.setTeam(Team.WHITE);
                updatePlayer(p);
                blackTeamNumber--;
                whiteTeamNumber++;
            }

        } else {
            // Move some white players to black team

            playersToMove = (int) Math.floor((whiteTeamNumber - blackTeamNumber) / 2);

            for (int i = 0; i < playersToMove; i++) {
                Player p = whiteTeam.pop();
                System.out.println("Moving player " + p.getNickname() + " to black team");
                p.setTeam(Team.BLACK);
                updatePlayer(p);
                whiteTeamNumber--;
                blackTeamNumber++;
            }

        }

        System.out.println("Players arranged! black: " + blackTeamNumber + " white: " + whiteTeamNumber);

        return true;
    }

    /**
     * Place current players inside generated world
     */
    private void placePlayersInsideWorld() {

        // randomly assign a player to a city

        // for each player
        for (Player p : players.values()) {

            // get a random city
            Random generator = new Random();
            Object[] values = world.getCities().values().toArray();
            City randomCity = (City) values[generator.nextInt(values.length)];

            // visit city (assign city to player)
            visitCity(p, randomCity);

            System.out.println("Assigning player " + p.getNickname() + " to city " + randomCity.getName());
        }

    }

    /**
     * Start player round timer
     */
    Player getNextPlayerRound() {

        synchronized (playerRoundLock) {

            if (playersQueue.isEmpty()) {

                if (getPlayersNumber() < 2) {
                    return null;
                }

                // I'm saving the players queue order here because concurrentHashMap doesn't guarantee ordering.
                if (originalPlayersQueue.isEmpty()) {
                    Collection<Player> playersCollection = players.values();
                    for (Player player : playersCollection) {
                        originalPlayersQueue.add(player.getId());
                    }
                }

                // if playersQueue is empty, restart players round.
                playersQueue = originalPlayersQueue;
            }

            UUID currentPlayerId = playersQueue.removeFirst();

            // if player has exited the game in the meanwhile...
            if (players.get(currentPlayerId) == null) {
                // remove player from queue
                for (int i = 0; i < playersQueue.size(); i++) {
                    UUID playerId = playersQueue.get(i);
                    if (playerId.equals(currentPlayerId)) {
                        playersQueue.remove(i);
                        break;
                    }
                }
                // get the next one
                return getNextPlayerRound();
            }

            currentPlayingPlayerId = currentPlayerId;

            return players.get(currentPlayerId);

        }

    }

    /**
     * Returns game round players
     *
     * @return
     */
    ConcurrentHashMap<UUID, Player> getPlayers() {
        return players;
    }

    /**
     * Subscribe a player to this round
     *
     * @param player
     */
    void subscribePlayer(Player player) throws RuntimeException {

        if (state != GameRoundState.MATCHMAKING) {
            throw new RuntimeException("Cannot subscribe, round already started or ended.");
        }

        if (playersNumber.get() == maxPlayersPerRound) {
            throw new RuntimeException("Cannot login, maximum number of players reached.");
        }

        Player inserted = players.putIfAbsent(player.getId(), player);

        if (inserted == null) {
            System.out.println("Subscribing player " + player.getNickname() + "(" + player.getId() + ")");
            System.out.println(players.size() + " players already subscribed");
            playersNumber.getAndIncrement();
        } else {
            System.out.println("Player already subscribed!");
        }

    }

    /**
     * Unsubscribe a player from this round
     *
     * @param playerId
     */
    void unsubscribePlayer(UUID playerId) {
        synchronized (playerRoundLock) {

            Player removed = players.remove(playerId);

            if (removed != null) {

                playersNumber.getAndDecrement();

                if (state == GameRoundState.STARTED) {

                    // check if there is enough players
                    if (getPlayersNumber() < 2) {
                        stopGameRound();
                        return;
                    }

                    // check if a team has 0 players
                    int white = 0;
                    int black = 0;
                    for (Player player : getPlayers().values()) {
                        if (player.getTeam().equals(Team.WHITE)) {
                            white++;
                        } else {
                            black++;
                        }
                    }

                    if (white == 0 || black == 0) {
                        // stop round
                        stopGameRound();
                        return;
                    }

                    // check if unsubscribed player can generate a deadlock
                    if (removed.getId().equals(currentPlayingPlayerId)) {
                        // avoid game deadlock
                        Player nextPlayer = getNextPlayerRound();
                        Game.getInstance().notifyPlayingPlayers(MessageActions.PLAYER_ROUND_START, nextPlayer.toJson());
                    }
                }
            }
        }
    }

    /**
     * Current players number
     *
     * @return
     */
    private int getPlayersNumber() {
        return playersNumber.get();
    }

    /**
     * Update (replace) a player
     *
     * @param player
     */
    private void updatePlayer(Player player) {
        try {
            players.replace(player.getId(), player);
        } catch (NullPointerException e) {
            // player has exited
        }
    }

    /**
     * Update (replace) a city
     *
     * @param city
     */
    private void updateCity(City city) {
        world.updateCity(city);
    }

    /**
     * Updates the player game round state with current round state
     */
    private void updatePlayersRoundState() {
        synchronized (playerRoundLock) {
            for (Player player : getPlayers().values()) {
                player.setRoundState(getState());
                players.replace(player.getId(), player);
            }
        }
    }

    /**
     * Move a player from his current city to another one
     * returns the ammo found in that city
     *
     * @param playerId
     * @param cityId
     */
    int movePlayer(UUID playerId, UUID cityId) throws RuntimeException {

        if (state != GameRoundState.STARTED) {
            throw new RuntimeException("GameRound not started yet or already ended.");
        }

        Player player = players.get(playerId);

        if (player == null) {
            throw new RuntimeException("Player not subscribed to this round.");
        }

        City currentPlayerCity = player.getCurrentCity();

        if (currentPlayerCity == null) {
            throw new RuntimeException("Player outside the world, this is weird...");
        }

        City nextPlayerCity = world.getCity(cityId);

        if (nextPlayerCity == null) {
            throw new RuntimeException("Specified city doesn't exists.");
        }

        if (currentPlayerCity.getId().equals(nextPlayerCity.getId())) {
            throw new RuntimeException("Cannot move inside the same city.");
        }

        if (!currentPlayerCity.isConnectedTo(nextPlayerCity)) {
            throw new RuntimeException("Cannot reach " + nextPlayerCity.getName() + " city from " + currentPlayerCity.getName());
        }

        // visit city
        return visitCity(player, nextPlayerCity);

    }

    /**
     * When a Jackeroo visit a city
     *
     * @param player
     * @param city
     * @return
     */
    private int visitCity(Player player, City city) {

        synchronized (playerRoundLock) {

            int foundAmmos = 0;

            // if city is visited for the very first time
            if (!city.isVisited()) {

                city.setVisited();
                foundAmmos = city.getAmmo();

                if (foundAmmos > 0) {
                    // update player ammo
                    player.increaseAmmo(foundAmmos);
                    // set city ammo to 0
                    city.consumeAmmo();
                    updateCity(city);
                }
            }


            // change player city
            player.setCurrentCity(city);

            if (uglyCurrentCity != null && city.getId().equals(uglyCurrentCity.getId())) {

                if (player.getAmmo() > 0) {
                    // ugly steals a player ammo!
                    player.decreaseAmmo(1);
                }

                // notify user
                JSONObject payload = new JSONObject();
                payload.put("ammo", player.getAmmo());
                Game.getInstance().notifyPlayer(player.getId(), MessageActions.UGLY_ENCOUNTERED, payload);
            }

            // update player
            updatePlayer(player);

            return foundAmmos;
        }

    }

    /**
     * Return the encountered enemy if exists
     *
     * @param player
     * @param cityId
     * @return
     */
    Player getEncounteredEnemy(Player player, UUID cityId) {

        for (Player p : getPlayers().values()) {

            if (p.getCurrentCity().getId().equals(cityId) && !p.getTeam().equals(player.getTeam())) {
                return p;
            }
        }

        return null;

    }

    /**
     * Returns players in a city
     *
     * @param cityId
     * @return
     */
    List<Player> getPlayersInCity(UUID cityId) {

        List<Player> playersInCity = new ArrayList<>();

        for (Player player : getPlayers().values()) {
            if (player.getCurrentCity().getId().equals(cityId)) {
                playersInCity.add(player);
            }
        }

        return playersInCity;
    }

    /**
     * Checks if a fight is occurring
     *
     * @return
     */
    private boolean isFighting() {
        return currentFight != null;
    }

    /**
     * returns current fight
     *
     * @return
     */
    Fight getFight() {
        return currentFight;
    }

    /**
     * start a fight
     *
     * @param cityId
     * @param questionId
     */
    void startFight(UUID cityId, int questionId) {
        if (!isFighting()) {
            currentFight = new Fight(cityId, questionId);
        }
    }

    /**
     * Adds a new battle (answer) to a fight
     *
     * @param playerId
     * @param answerId
     */
    void addBattleToFight(UUID playerId, int answerId) throws RuntimeException {
        synchronized (playerRoundLock) {

            if (state != GameRoundState.STARTED) {
                throw new RuntimeException("Game not already started or ended.");
            }

            Player player = getPlayers().get(playerId);
            currentFight.addBattle(player, answerId);
        }
    }

    /**
     * Checks if all battles in a fight are performed
     *
     * @return
     */
    boolean areAllBattlesPerformed() {
        return playersNumber.get() == currentFight.getBattlesPerformed();
    }

    /**
     * Returns the team that won the fight,
     * returns null in case of draw
     *
     * @return Team|null
     */
    Team getFightWinner() {

        // if every player has performed a battle

        Team winner = currentFight.getWinner();

        if (winner != null) {

            // get player in the city where the fight started
            List<Player> playersInCity = getPlayersInCity(currentFight.getCityId());
            List<Player> winnerPlayersInCity = new ArrayList<>();
            int stolenAmmo = 0;

            // stole ammos and count winner players in city
            for (Player p : playersInCity) {
                if (!winner.equals(p.getTeam())) {
                    // this player lose
                    int loserAmmo = p.getAmmo();
                    if (loserAmmo > 0) {
                        stolenAmmo += loserAmmo;
                        p.decreaseAmmo(loserAmmo);
                        updatePlayer(p);
                    }
                } else {
                    winnerPlayersInCity.add(p);
                }
            }

            // distribute stolen ammo evenly between winning players
            if (stolenAmmo > 0) {

                int winnerPlayersNum = winnerPlayersInCity.size();
                int earnedAmmoPerPlayer = (stolenAmmo / winnerPlayersNum);
                int remainingAmmo = (stolenAmmo % winnerPlayersNum);

                for (int i = 0; i < winnerPlayersNum; i++) {
                    int extra = (i <= remainingAmmo) ? 1 : 0;
                    Player p = winnerPlayersInCity.get(i);
                    p.increaseAmmo(earnedAmmoPerPlayer + extra);
                    updatePlayer(p);
                }
            }

        }
        // else it's a draw

        // end this fight
        currentFight = null;

        return winner;
    }

    /**
     * Return the round results
     * (team having greater number of ammo)
     *
     * @return Team|null
     */
    private JSONObject getRoundResults() {

        int whiteTeamAmmo = 0;
        int blackTeamAmmo = 0;

        for (Player player : getPlayers().values()) {

            int playerAmmo = player.getAmmo();

            if (player.getTeam().equals(Team.WHITE)) {
                whiteTeamAmmo += playerAmmo;
            } else {
                blackTeamAmmo += playerAmmo;
            }
        }

        boolean isDraw = whiteTeamAmmo == blackTeamAmmo;

        JSONObject res = new JSONObject();
        res.put(Team.WHITE.toString(), whiteTeamAmmo);
        res.put(Team.BLACK.toString(), blackTeamAmmo);
        res.put("isDraw", isDraw);
        if (!isDraw) {
            res.put("winnerTeam", whiteTeamAmmo > blackTeamAmmo ? Team.WHITE : Team.BLACK);
        }

        return res;
    }

    /**
     * Generate vintage random World (as a graph)
     *
     * @throws RuntimeException
     */
    private void generateWorld() throws RuntimeException {

        if (state != GameRoundState.IDLE) {
            System.out.println("Cannot generate word, round already started or ended");
            throw new RuntimeException("Cannot generate word, round already started or ended");
        }

        int playersNumber = getPlayersNumber();
        int citiesNumber = rand((int) Math.round(playersNumber * 1.5), playersNumber * 2);
        int maxAmmoPerCity = playersNumber * 5;
        int maxConnectionsPerCity = rand(1, Math.round(citiesNumber / 3));
        if (maxConnectionsPerCity > 4) {
            maxConnectionsPerCity = 4;
        }

        // generate cities from random Old Wild West names

        List<City> cities = new ArrayList<>();
        List<String> cityNames = WildWestCityNames.getRandomNames(citiesNumber);

        for (String cityName : cityNames) {
            cities.add(new City(cityName, "", rand(0, maxAmmoPerCity)));
        }

        // generate random connections between cities
        for (int i = 0; i < cities.size(); i++) {

            int maxConnections = rand(1, maxConnectionsPerCity);
            HashSet<Integer> randCities = new HashSet<>();
            Random random = new Random();
            int j = 0;
            int randI = 0;

            City city = cities.get(i);
            City nextCity;

            if (i < citiesNumber - 1) {
                nextCity = cities.get(i + 1);
                randI = i + 1;
            } else {
                nextCity = cities.get(0);
            }

            // avoid path to itself
            randCities.add(i);

            // ensure connections between all cities (circular)
            randCities.add(randI);
            nextCity.addConnection(city);
            city.addConnection(nextCity);

            // add some more connections between cities
            int guard = 0;
            while (j < maxConnections && guard < 100) {

                randI = random.nextInt(citiesNumber);

                if (!randCities.contains(randI)) {
                    randCities.add(randI);
                    // two way connection
                    city.addConnection(cities.get(randI));
                    cities.get(randI).addConnection(city);
                    j++;
                }

                guard++;
            }

            world.addCity(city);
        }

    }

    /**
     * Return game round state
     *
     * @return
     */
    private GameRoundState getState() {
        return state;
    }

    /**
     * Ugly player task
     */
    private Runnable uglyTask = new Runnable() {

        @Override
        public void run() {

            ArrayList<City> cities = new ArrayList<>(world.getCities().values());
            uglyCurrentCity = cities.get(0);
            Random random = new Random();
            int randMin = Math.round(TimeUnit.MINUTES.toMillis(Game.GAME_ROUND_DURATION) / 20); // if round is 10 min, randMin will be 0.5
            int randMax = Math.round(TimeUnit.MINUTES.toMillis(Game.GAME_ROUND_DURATION) / 4);  // if round is 10 min, randMax will be 2.5
            if (randMin == 0) {
                randMin = 1;
            }
            if (randMax == 0) {
                randMax = 2;
            }

            // while a round is active...
            while (getState().equals(GameRoundState.STARTED)) {

                long waitInCity = rand(randMin, randMax);
                ArrayList<City> connections = new ArrayList<>(uglyCurrentCity.getConnections().values());
                City nextCity = connections.get(random.nextInt(connections.size()));

                try {
                    // wait in current city
                    System.out.println("Ugly: waiting in city " + nextCity.getName() + " for " + waitInCity + " millis");
                    Thread.sleep(waitInCity);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Ugly: moving to city " + nextCity.getName());

                // move to another city
                moveToCity(nextCity);
            }

            System.out.println("Ugly: Round ended.");
        }

        /**
         * Move Ugly to a city and perform its behavior on players found
         * @param city
         */
        private void moveToCity(City city) {
            synchronized (playerRoundLock) {

                if (city == null) {
                    return;
                } else if (!uglyCurrentCity.isConnectedTo(city)) {
                    return;
                }

                uglyCurrentCity = city;
                List<Player> players = getPlayersInCity(city.getId());

                if (players.size() == 0) {
                    System.out.println("Ugly: no players found in " + city.getName());
                    return;
                }

                // some players found...
                for (Player player : players) {

                    System.out.println("Ugly: encountered player " + player.getNickname());

                    if (player.getAmmo() > 0) {

                        // ugly steals a player ammo!
                        player.decreaseAmmo(1);
                        updatePlayer(player);

                        JSONObject payload = new JSONObject();
                        payload.put("ammo", player.getAmmo());
                        Game.getInstance().notifyPlayer(player.getId(), MessageActions.UGLY_ENCOUNTERED, payload);
                    }
                }
            }
        }
    };

    /**
     * Start the Ugly task as a new thread
     */
    private void startUgly() {
        new Thread(uglyTask).start();
    }

    // utils

    private static int rand(int from, int to) {
        return (int) Math.round(from + Math.random() * to);
    }

}
