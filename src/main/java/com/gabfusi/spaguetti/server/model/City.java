package com.gabfusi.spaguetti.server.model;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Hashtable;
import java.util.UUID;

/**
 * Models a graph node
 */
class City {

    private UUID id;
    private String name;
    private String image;
    private Hashtable<UUID, City> connections;

    private volatile int ammo;
    private volatile boolean visited = false;

    /**
     * @return
     */
    UUID getId() {
        return id;
    }

    /**
     * @param name
     * @param image
     * @param ammo
     */
    City(String name, String image, int ammo) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.image = image;
        this.ammo = ammo;
        this.connections = new Hashtable<>();
    }

    /**
     * @return
     */
    Hashtable<UUID, City> getConnections() {
        return connections;
    }

    /**
     * @param city
     */
    void addConnection(City city) {
        this.connections.putIfAbsent(city.getId(), city);
    }

    /**
     * @param city
     * @return
     */
    boolean isConnectedTo(City city) {
        return this.connections.get(city.getId()) != null;
    }

    /**
     * @return
     */
    String getName() {
        return name;
    }

    /**
     * @return
     */
    String getImage() {
        return image;
    }

    /**
     * @return
     */
    boolean isVisited() {
        return visited;
    }

    /**
     *
     */
    void setVisited() {
        visited = true;
    }

    /**
     * @return
     */
    int getAmmo() {
        return ammo;
    }

    /**
     *
     */
    void consumeAmmo() {
        ammo = 0;
    }

    /**
     * @return
     */
    JSONObject toJson() {
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();

        obj.put("id", this.id);
        obj.put("name", this.name);
        obj.put("image", this.image);

        for (City city : this.connections.values()) {
            JSONObject c = new JSONObject();
            c.put("id", city.getId());
            c.put("name", city.getName());
            arr.put(c);
        }

        obj.put("connections", arr);

        return obj;
    }
}
