package com.gabfusi.spaguetti.server.model;

import com.gabfusi.spaguetti.server.wildwest.WildWestQuiz;
import com.gabfusi.spaguetti.server.networking.Message;
import com.gabfusi.spaguetti.server.networking.MessageActions;
import com.gabfusi.spaguetti.server.networking.socket.SocketServer;
import org.json.JSONObject;

import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Game {

    private static final int GAME_ROUND_INTERVAL = 4; // minutes
    static final int GAME_ROUND_DURATION = 2; // minutes
    private static final int GAME_TIME_INTERVAL = 10; // seconds

    private final WildWestQuiz fightQuiz = new WildWestQuiz();
    private final ScheduledExecutorService gameScheduler = Executors.newSingleThreadScheduledExecutor();
    private final ScheduledExecutorService timeScheduler = Executors.newSingleThreadScheduledExecutor();
    private GameRound currentRound = null;
    private GameRound scheduledRound = null;
    private long upcomingRoundStartTick;
    private SocketServer socketServer;
    private int maxPlayersPerRound;

    private static final Object roundLock = new Object();

    /**
     * Game is a singleton
     */
    private Game() {
    }

    // using the  Initialization-on-demand holder idiom
    // see https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom

    private static class LazyHolder {
        static final Game INSTANCE = new Game();
    }

    public static Game getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * Starts the game
     *
     * @param maxPlayersPerRound
     */
    public void start(int maxPlayersPerRound) {
        this.maxPlayersPerRound = maxPlayersPerRound;
        scheduledRound = new GameRound(GAME_ROUND_DURATION, maxPlayersPerRound);
        startTimeScheduler();
        startRoundSchedule();
    }

    /**
     * @param playerId
     * @return
     */
    public JSONObject getRoundStatusForPlayer(UUID playerId) {
        Player player = getPlayer(playerId);
        System.out.println(playerId);
        return getRoundStatus(player.getRoundState());
    }

    /**
     * @param state
     * @return
     */
    private JSONObject getRoundStatus(GameRoundState state) {

        switch (state) {

            case MATCHMAKING:
                return scheduledRound.getStatus();

            case IDLE:
            case STARTED:
                return currentRound.getStatus();

        }

        return null;
    }

    /**
     * Send a broadcast message to all players subscribed to the scheduled round
     *
     * @param actionType
     * @param payload
     */
    public void notifyWaitingPlayers(MessageActions actionType, JSONObject payload) {
        Message message = new Message();
        message.setAction(actionType);
        message.setPayload(payload);

        for (Player player : scheduledRound.getPlayers().values()) {
            this.socketServer.sendTo(player.getId(), message.toJson());
        }
    }

    /**
     * Send a broadcast message to all players subscribed to the current round
     *
     * @param actionType
     * @param payload
     */
    public void notifyPlayingPlayers(MessageActions actionType, JSONObject payload) {
        Message message = new Message();
        message.setAction(actionType);
        message.setPayload(payload);

        for (Player player : currentRound.getPlayers().values()) {
            this.socketServer.sendTo(player.getId(), message.toJson());
        }
    }

    /**
     * Send a broadcast message to all the connected clients
     *
     * @param actionType
     * @param payload
     */
    private void notifyPlayers(MessageActions actionType, JSONObject payload) {
        Message message = new Message();
        message.setAction(actionType);
        message.setPayload(payload);
        this.socketServer.broadcast(message.toJson());
    }

    /**
     * Notify players of a team
     *
     * @param team
     * @param actionType
     * @param payload
     */
    public void notifyPlayersOfTeam(Team team, MessageActions actionType, JSONObject payload) {
        Message message = new Message();
        message.setAction(actionType);
        message.setPayload(payload);

        for (Player player : currentRound.getPlayers().values()) {

            if (player.getTeam().equals(team)) {
                this.socketServer.sendTo(player.getId(), message.toJson());
            }
        }
    }

    /**
     * Send a message to a specified player
     *
     * @param playerId
     * @param actionType
     * @param payload
     */
    void notifyPlayer(UUID playerId, MessageActions actionType, JSONObject payload) {
        Message message = new Message();
        message.setAction(actionType);
        message.setPayload(payload);
        this.socketServer.sendTo(playerId, message.toJson());
    }

    /**
     * Round scheduler task
     */
    private final Runnable gameRoundRotation = () -> {

        boolean roundCanStart;

        synchronized (roundLock) {

            System.out.println("Starting round...");

            // make the scheduled round as the current one.
            currentRound = scheduledRound;
            roundCanStart = currentRound.startGameRound();

            // schedule next round
            scheduledRound = new GameRound(GAME_ROUND_DURATION, maxPlayersPerRound);

            // upcoming round start/end tick
            upcomingRoundStartTick = GameTime.getTime() + (GAME_ROUND_INTERVAL * 60 * 1000);

        }

        // start scheduled round
        if (roundCanStart) {

            // notify waiting players
            notifyPlayingPlayers(MessageActions.ROUND_START, currentRound.getStatus());

            // notify players about their ammos
            for (Player p : currentRound.getPlayers().values()) {
                if (p.getAmmo() > 0) {
                    // notify player
                    JSONObject playerJson = p.toJson();
                    playerJson.put("ammo", p.getAmmo());
                    notifyPlayer(p.getId(), MessageActions.AMMO_FOUND, playerJson);
                }
            }

            // Start first player round
            Player currentPlayer = currentRound.getNextPlayerRound();

            if (currentPlayer == null) {
                // no players
                currentRound.stopGameRound();
            } else {
                notifyPlayingPlayers(MessageActions.PLAYER_ROUND_START, currentPlayer.toJson());
            }

        } else {

            // TODO reschedule next round in 1 minute

            // notify waiting players
            JSONObject payload = new JSONObject();
            payload.put("message", "Cannot start game, you're the only player in this round"); // , please wait for other players...
            notifyPlayingPlayers(MessageActions.ERROR, payload);

            System.out.print("Stopping current round, there are no players!");
            currentRound.silentStopGameRound();
            currentRound = null;
        }

    };

    /**
     * Start Game Round scheduling
     */
    private void startRoundSchedule() {

        // start scheduled round
        gameScheduler.scheduleAtFixedRate(gameRoundRotation, GAME_ROUND_INTERVAL, GAME_ROUND_INTERVAL, TimeUnit.MINUTES);

        // upcoming round start/end tick
        upcomingRoundStartTick = GameTime.getTime() + (GAME_ROUND_INTERVAL * 60 * 1000);

    }

    /**
     * Update time ticker
     */
    private void startTimeScheduler() {

        final Runnable gameTimeScheduler = () -> {

            try {
                JSONObject gameTime = GameTime.getTimeJson();
                gameTime.put("next_round_at", upcomingRoundStartTick);

                System.out.println("game time tick : " + gameTime.getLong("time"));
                notifyPlayers(MessageActions.GAME_TIME, gameTime);
            } catch (Exception e) {
                e.printStackTrace();
            }

        };

        timeScheduler.scheduleAtFixedRate(gameTimeScheduler, 0, GAME_TIME_INTERVAL, TimeUnit.SECONDS);

    }

    /**
     * Subscribe a player to the scheduled game round
     *
     * @param nickname
     * @param team
     */
    public void subscribePlayer(UUID playerId, String nickname, Team team) throws RuntimeException {
        // create a new player

        Player player = new Player(playerId, nickname, team);
        scheduledRound.subscribePlayer(player);
    }

    /**
     * Unsubscribe a player
     *
     * @param playerId
     */
    public void unsubscribePlayer(UUID playerId) {
        Player player = getPlayer(playerId);

        if (player != null) {

            if (player.getRoundState().equals(GameRoundState.MATCHMAKING)) {
                scheduledRound.unsubscribePlayer(playerId);
            } else {
                currentRound.unsubscribePlayer(playerId);
            }

        }
    }

    /**
     * search a player in both scheduled and current round
     *
     * @param playerId
     * @return null|Player
     */
    public Player getPlayer(UUID playerId) {

        Player found;

        if (currentRound != null) {
            found = currentRound.getPlayers().get(playerId);
            if (found != null) {
                return found;
            }
        }

        found = scheduledRound.getPlayers().get(playerId);

        return found;

    }

    /**
     * @param playerId
     * @param cityId
     */
    public void movePlayer(UUID playerId, UUID cityId) throws RuntimeException {

        synchronized (roundLock) {

            if (!currentRound.isGameRoundStarted()) {
                throw new RuntimeException("GameRound not started yet or already ended.");
            }

            Player player = getPlayer(playerId);

            // move the player and collect ammo (if first visit)
            int foundAmmo = currentRound.movePlayer(playerId, cityId); // throws

            if (foundAmmo > 0) {
                // notify player
                JSONObject playerJson = player.toJson();
                playerJson.put("ammo", player.getAmmo());
                notifyPlayer(playerId, MessageActions.AMMO_FOUND, playerJson);
            }

            // check if there is another player in the city
            Player enemy = currentRound.getEncounteredEnemy(player, cityId);

            // if there is another player (of a different team) than start the fight
            if (enemy != null) {
                // start fight

                notifyPlayer(player.getId(), MessageActions.ENEMY_FOUND, enemy.toJson());

                notifyPlayingPlayers(MessageActions.PLAYER_MOVE, currentRound.getStatus());

                // start a fight
                JSONObject question = fightQuiz.getRandomQuestion();
                currentRound.startFight(cityId, question.getInt("questionId"));
                notifyPlayingPlayers(MessageActions.FIGHT_START, question);

            } else {
                // player round end

                // notify that current player round has ended (the PLAYER_MOVE is implicit in this case)
                notifyPlayingPlayers(MessageActions.PLAYER_ROUND_END, currentRound.getStatus());

                // start a new player round
                Player currentPlayer = currentRound.getNextPlayerRound();
                if (currentPlayer == null) {
                    // no players
                    currentRound.stopGameRound();
                } else {
                    notifyPlayingPlayers(MessageActions.PLAYER_ROUND_START, currentPlayer.toJson());
                }
            }

        }

    }

    /**
     * Add a battle (answered question) to current fight
     *
     * @param playerId
     * @param answerId
     */
    public void addBattleToCurrentFight(UUID playerId, int answerId) {

        currentRound.addBattleToFight(playerId, answerId);

        // on fight end
        if (currentRound.areAllBattlesPerformed()) {

            Fight fight = currentRound.getFight();
            Team winnerTeam = currentRound.getFightWinner(); // if null it's a draw

            JSONObject gameRoundStatus = currentRound.getStatus();

            System.out.println("getFightWinner result: " + winnerTeam);

            if (winnerTeam == null) {

                gameRoundStatus.put("winnerTeam", "draw");

            } else {

                gameRoundStatus.put("winnerTeam", winnerTeam);

                for (Player p : currentRound.getPlayersInCity(fight.getCityId())) {
                    JSONObject ammos = new JSONObject();
                    ammos.put("ammo", p.getAmmo());
                    notifyPlayer(p.getId(), MessageActions.AMMO_UPDATED, ammos);
                }

            }

            notifyPlayingPlayers(MessageActions.FIGHT_END, gameRoundStatus);
        }
    }

    /**
     * Return current game time
     *
     * @return
     */
    public long getGameTime() {
        return GameTime.getTime();
    }

    /**
     * Return the next game round time
     *
     * @return
     */
    public long getUpcomingRoundStartTick() {
        return upcomingRoundStartTick;
    }


    public void setSocketServer(SocketServer socketServer) {
        this.socketServer = socketServer;
    }
}
