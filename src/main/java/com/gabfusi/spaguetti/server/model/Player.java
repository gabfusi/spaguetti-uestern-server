package com.gabfusi.spaguetti.server.model;

import org.json.JSONObject;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class Player {

    private final UUID id;
    private final String nickname;

    private volatile Team team; // may change in game round start process
    private volatile City currentCity = null;
    private volatile int ammo = 0;
    private volatile GameRoundState roundState = GameRoundState.MATCHMAKING;

    private final Object ammoLock = new Object();

    Player(UUID id, String nickname, Team team) {
        this.id = id;
        this.nickname = nickname;
        this.team = team;
    }

    UUID getId() {
        return id;
    }

    String getNickname() {
        return nickname;
    }

    Team getTeam() {
        return team;
    }

    void setTeam(Team t) {
        team = t;
    }

    int getAmmo() {
        return ammo;
    }

    City getCurrentCity() {
        return currentCity;
    }

    void setCurrentCity(City city) {
        currentCity = city;
    }

    public GameRoundState getRoundState() {
        return roundState;
    }

    void setRoundState(GameRoundState roundState) {
        this.roundState = roundState;
    }

    void increaseAmmo(int number) {
        synchronized (ammoLock) {
            ammo += number;
        }
    }

    void decreaseAmmo(int number) {
        synchronized (ammoLock) {
            if (number > ammo) {
                ammo = 0;
            } else {
                ammo -= number;
            }
        }
    }

    JSONObject toJson() {
        JSONObject obj = new JSONObject();

        obj.put("id", this.id);
        obj.put("nickname", this.nickname);
        obj.put("team", this.team);

        if (this.currentCity != null) {
            obj.put("currentCityId", this.currentCity.getId());
        }

        return obj;
    }

}
