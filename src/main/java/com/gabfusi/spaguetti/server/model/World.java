package com.gabfusi.spaguetti.server.model;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Hashtable;
import java.util.UUID;

/**
 * Models a graph of nodes (cities)
 */
class World {

    private final Hashtable<UUID, City> cities;

    /**
     * Models a world
     */
    World() {
        cities = new Hashtable<>();
    }

    /**
     *
     * @param city
     */
    void addCity(City city) {
        cities.put(city.getId(), city);
    }

    /**
     *
     * @param cityId
     * @return
     */
    City getCity(UUID cityId) {
        return cities.get(cityId);
    }

    /**
     *
     * @param city
     */
    void updateCity(City city) {
        if (getCity(city.getId()) != null) {
            cities.replace(city.getId(), city);
        }
    }

    /**
     *
     * @return
     */
    Hashtable<UUID, City> getCities() {
        return cities;
    }

    /**
     *
     * @return
     */
    JSONObject toJson() {

        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();

        for (City city : cities.values()) {
            arr.put(city.toJson());
        }

        obj.put("cities", arr);

        return obj;
    }

}
