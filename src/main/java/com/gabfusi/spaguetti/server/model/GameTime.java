package com.gabfusi.spaguetti.server.model;

import org.json.JSONObject;

class GameTime {

    static JSONObject getTimeJson() {
        JSONObject wrap = new JSONObject();
        wrap.put("time", getTime());
        return wrap;
    }

    static long getTime() {
        return System.currentTimeMillis();
    }

}
